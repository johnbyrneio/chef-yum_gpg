name             'yum_gpg'
maintainer       'John Byrne'
maintainer_email 'johnpbyrne <at> gmail <dot> com'
license          'All rights reserved'
description      'Provides yum_key LWRP removed from community yum 3.x cookbook'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.0'

%w{ redhat centos scientific amazon }.each do |os|
  supports os, '>= 5.0'
end
